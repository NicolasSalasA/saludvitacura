export const ordenarAlfabeticamente = (stringA, stringB, order = 'asc') => {
    switch (order.toLowerCase()) {
        case 'asc':
            return stringA.localeCompare(stringB)
        case 'desc':
            return stringB.localeCompare(stringA)
        default:
            console.warn(`No especificaste un orden valido. Los valores pueden ser: 'asc' para orden ascendente y 'desc' para orden descendente. Este parametro no discrimina mayusculas y minusculas.`)
            return null;
    }
}