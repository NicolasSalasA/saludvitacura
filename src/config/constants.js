//Constantes
const DEV = true;
export const URL_API = !DEV ? 'http://vitacura-back.xxmjamcez7.us-west-2.elasticbeanstalk.com' : 'http://localhost:3000';

//Auth actions
export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAIL = 'LOGIN_USER_FAIL';
export const LOGOUT = 'LOGOUT';

