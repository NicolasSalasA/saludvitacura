import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import { offline } from '@redux-offline/redux-offline';
import offlineConfig from '@redux-offline/redux-offline/lib/defaults';
import logger from 'redux-logger';
import reducers from '../reducers';

const store = createStore(
    reducers,
    {},
    compose(offline(offlineConfig), applyMiddleware(ReduxThunk, logger))
);

export default store;