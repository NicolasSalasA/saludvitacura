import axios from 'axios';
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGOUT,
    URL_API
} from '../config/constants';
import { RESET_STATE } from "@redux-offline/redux-offline/lib/constants";


export const loginUser = () => ({ type: LOGIN_USER });

export const logOut = () => (dispatch) => {
    dispatch({ type: LOGOUT });
};
