import React, { Component } from 'react'
import { connect } from 'react-redux'
import { loginUser } from '../../actions';
import SALTALA_LOGO from '../../assets/img/clinicatabancura.png';
import './style.css'

class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: ''
        }
    }

    render() {
        return (
            <div className={'login-main-container'}>
                <div className="login-container">
                    <div className="login-logo-area">
                        <img
                            src={SALTALA_LOGO}
                            alt="logo saltala"
                            className={'login-logo'}
                        />
                    </div>
                    <div className="login-form-area">
                        <p className={'login-wellcome'}>¡BIENVENIDO A RED SALUD!</p>
                        <p className={'login-instructions'}>
                            Ingresa tus datos para iniciar sesión
            </p>
                        <input
                            className={'saltala-input'}
                            placeholder={'Usuario'}
                            type="text"
                            name={'email'}
                            onChange={({ target: { value: email } }) => this.setState({ email })}
                        />
                        <input
                            className={'saltala-input'}
                            placeholder={'Contraseña'}
                            type="password"
                            name={'password'}
                            onChange={({ target: { value: password } }) => this.setState({ password })}
                        />
                        <button className={'login-saltala-button'} onClick={() => this.props.loginUser()}>Iniciar sesión</button>
                    </div>
                </div>
            </div>
        )
    }
}


export default connect(null, { loginUser })(Login)