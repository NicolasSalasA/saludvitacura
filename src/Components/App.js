import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from '../config/store';
import moment from 'moment';
import esLocale from 'moment/locale/es';
import MainComponent from './Container';


moment.locale('es', esLocale);
moment.locale('es');

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <MainComponent />
            </Provider>
        );
    }
}

export default App;