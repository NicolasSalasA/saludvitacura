import React, { Component } from 'react';
import { Table, Button } from 'antd';
import _ from 'lodash';
import { CSVLink } from 'react-csv';
import { headers, ordenarAlfebeticamente, downloadButton } from './helpers';
import 'antd/dist/antd.css';
import './style.css';


class Medico extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredInfo: {},
      sortedInfo: {
        columnKey: props.columnas[0],
        order: 'descend',
      },
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps !== this.props || nextState !== this.state;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.data != nextProps.data
      || this.props.columnas != nextProps.columnas) {
      this.setState({
        filteredInfo: {},
        sortedInfo: {
          columnKey: nextProps.columnas[0],
          order: 'descend',
        },
      })
    }
  }

  reordenar = (pagination, filters, sorter) => {
    console.log(filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  }

  render() {
    let { data, especialidades, columnas } = this.props;
    let { sortedInfo = {}, filteredInfo = {} } = this.state;
    const semaforo = {
      1: 'Verde',
      2: 'Amarillo',
      3: 'Rojo'
    }
    let filters = {};

    data = data.map((elem, key) => ({ ...elem, key }));
    columnas = ["Nombre", ...columnas]
    if (especialidades.length > 0) {
      especialidades = especialidades.map(especialidad => ({ text: especialidad, value: especialidad }))
      filters = {
        filters: [{
          text: 'Por especialidad',
          value: 'especialidad',
          children: especialidades
        }],
        filteredValue: filteredInfo.Nombre || null,
        onFilter: (value, record) => record.Nombre === value

      }
    } else {
      const letras = _.keys(
        _.countBy(data, (value) => value.Nombre[0].toLowerCase()))
        .map(letra => ({ text: letra.toUpperCase(), value: letra }))
      filters = {
        filters: [{
          text: 'Por inicial',
          value: 'inicial',
          children: letras
        }],
        filteredValue: filteredInfo.Nombre || null,
        onFilter: (value, record) => record.Nombre[0].toLowerCase() == value

      }
    }

    const columns = [
      {
        title: 'Nombre',
        dataIndex: 'Nombre',
        key: 'Nombre',
        ...filters
      },
      {
        title: 'Programados',
        dataIndex: 'Programados',
        key: 'Programados',
        sorter: (a, b) => a.Programados - b.Programados,
        sortOrder: sortedInfo.columnKey === 'Programados' && sortedInfo.order,
      },
      {
        title: 'Bloqueados',
        dataIndex: 'Bloqueados',
        key: 'Bloqueados',
        sorter: (a, b) => a.Bloqueados - b.Bloqueados,
        sortOrder: sortedInfo.columnKey === 'Bloqueados' && sortedInfo.order,
      },
      {
        title: '%Bloqueos',
        dataIndex: 'porcenBloqueados',
        key: 'porcenBloqueados',
        render: (value) => <p>{value.valor}<span className={`semaforo semaforo${semaforo[value.semaforo]}`}></span></p>,
        sorter: (a, b) => a.porcenBloqueados.valor.split('%')[0] - b.porcenBloqueados.valor.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenBloqueados' && sortedInfo.order,
      },
      {
        title: 'Disponibles',
        dataIndex: 'Disponibles',
        key: 'Disponibles',
        sorter: (a, b) => a.Disponibles - b.Disponibles,
        sortOrder: sortedInfo.columnKey === 'Disponibles' && sortedInfo.order,
      },
      {
        title: 'Asignados',
        dataIndex: 'Asignados',
        key: 'Asignados',
        sorter: (a, b) => a.Asignados - b.Asignados,
        sortOrder: sortedInfo.columnKey === 'Asignados' && sortedInfo.order,
      },
      {
        title: 'T. Cirugias',
        dataIndex: 'cirugias',
        key: 'cirugias',
        sorter: (a, b) => a.cirugias - b.cirugias,
        sortOrder: sortedInfo.columnKey === 'cirugias' && sortedInfo.order,
      },
      {
        title: 'C. Programadas',
        dataIndex: 'programadas',
        key: 'programadas',
        sorter: (a, b) => a.programadas - b.programadas,
        sortOrder: sortedInfo.columnKey === 'programadas' && sortedInfo.order,
      },
      {
        title: 'C. Urgencias',
        dataIndex: 'urgencias',
        key: 'urgencias',
        sorter: (a, b) => a.urgencias - b.urgencias,
        sortOrder: sortedInfo.columnKey === 'urgencias' && sortedInfo.order,
      },
      {
        title: 'Ecotomografía Obstétrica',
        dataIndex: 'ecoG',
        key: 'ecoG',
        sorter: (a, b) => a.ecoG - b.ecoG,
        sortOrder: sortedInfo.columnKey === 'ecoG' && sortedInfo.order,
      },
      {
        title: 'Mamografia',
        dataIndex: 'mamo',
        key: 'mamo',
        sorter: (a, b) => a.mamo - b.mamo,
        sortOrder: sortedInfo.columnKey === 'mamo' && sortedInfo.order,
      },
      {
        title: 'Resonancia Magnética',
        dataIndex: 'rnm',
        key: 'rnm',
        sorter: (a, b) => a.rnm - b.rnm,
        sortOrder: sortedInfo.columnKey === 'rnm' && sortedInfo.order,
      },
      {
        title: 'Scanner',
        dataIndex: 'tac',
        key: 'tac',
        sorter: (a, b) => a.tac - b.tac,
        sortOrder: sortedInfo.columnKey === 'tac' && sortedInfo.order,
      },
      {
        title: 'Radiografías',
        dataIndex: 'rx',
        key: 'rx',
        sorter: (a, b) => a.rx - b.rx,
        sortOrder: sortedInfo.columnKey === 'rx' && sortedInfo.order,
      },
      {
        title: 'Ecografía',
        dataIndex: 'eco',
        key: 'eco',
        sorter: (a, b) => a.eco - b.eco,
        sortOrder: sortedInfo.columnKey === 'eco' && sortedInfo.order,
      },
      {
        title: 'Densitometría',
        dataIndex: 'dens',
        key: 'dens',
        sorter: (a, b) => a.dens - b.dens,
        sortOrder: sortedInfo.columnKey === 'dens' && sortedInfo.order,
      },
      {
        title: '%Asignación',
        dataIndex: 'porcenAsignacion',
        key: 'porcenAsignacion',
        render: (value) => <p>{value.valor}<span className={`semaforo semaforo${semaforo[value.semaforo]}`}></span></p>,
        sorter: (a, b) => a.porcenAsignacion.valor.split('%')[0] - b.porcenAsignacion.valor.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenAsignacion' && sortedInfo.order,
      },
      {
        title: 'Ausentes',
        dataIndex: 'Ausentes',
        key: 'Ausentes',
        sorter: (a, b) => a.Ausentes - b.Ausentes,
        sortOrder: sortedInfo.columnKey === 'Ausentes' && sortedInfo.order,
      },
      {
        title: '%Ausentismo',
        dataIndex: 'porcenAusentismo',
        key: 'porcenAusentismo',
        render: (value) => <p>{value.valor}<span className={`semaforo semaforo${semaforo[value.semaforo]}`}></span></p>,
        sorter: (a, b) => a.porcenAusentismo.valor.split('%')[0] - b.porcenAusentismo.valor.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenAusentismo' && sortedInfo.order,
      },
      {
        title: 'Sobrecupo',
        dataIndex: 'Sobrecupo',
        key: 'Sobrecupo',
        sorter: (a, b) => a.Sobrecupo - b.Sobrecupo,
        sortOrder: sortedInfo.columnKey === 'Sobrecupo' && sortedInfo.order,
      },
      {
        title: '%Sobrecupo',
        dataIndex: 'porcenSobrecupo',
        key: 'porcenSobrecupo',
        render: (value) => <p>{value.valor}<span className={`semaforo semaforo${semaforo[value.semaforo]}`}></span></p>,
        sorter: (a, b) => a.porcenSobrecupo.valor.split('%')[0] - b.porcenSobrecupo.valor.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenSobrecupo' && sortedInfo.order,
      },
      {
        title: 'Vendidos',
        dataIndex: 'Recaudados',
        key: 'Recaudados',
        sorter: (a, b) => a.Recaudados - b.Recaudados,
        sortOrder: sortedInfo.columnKey === 'Recaudados' && sortedInfo.order,
      },
      {
        title: '%Vendido / Programado',
        dataIndex: 'porcenVendidoProgramado',
        key: 'porcenVendidoProgramado',
        sorter: (a, b) => a.porcenVendidoProgramado.split('%')[0] - b.porcenVendidoProgramado.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenVendidoProgramado' && sortedInfo.order,
      },
      {
        title: '%Vendido / Disponible',
        dataIndex: 'porcenVendidoDisponible',
        key: 'porcenVendidoDisponible',
        sorter: (a, b) => a.porcenVendidoDisponible.split('%')[0] - b.porcenVendidoDisponible.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenVendidoDisponible' && sortedInfo.order,
      },
      {
        title: '%Cirugias / Vendidos',
        dataIndex: 'porcenCirugias',
        key: 'porcenCirugias',
        sorter: (a, b) => a.porcenCirugias.split('%')[0] - b.porcenCirugias.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenCirugias' && sortedInfo.order,
      },
      {
        title: '%Examenes / Vendidos',
        dataIndex: 'porcenExamenes',
        key: 'porcenExamenes',
        sorter: (a, b) => a.porcenExamenes.split('%')[0] - b.porcenExamenes.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenExamenes' && sortedInfo.order,
      },
      {
        title: 'T. Examenes',
        dataIndex: 'totalExamenes',
        key: 'totalExamenes',
        sorter: (a, b) => a.totalExamenes.split('%')[0] - b.totalExamenes.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'totalExamenes' && sortedInfo.order,
      },
      {
        title: 'Procedimientos',
        dataIndex: 'procedimientos',
        key: 'procedimientos',
        sorter: (a, b) => a.procedimientos - b.procedimientos,
        sortOrder: sortedInfo.columnKey === 'procedimientos' && sortedInfo.order,
      },
      {
        title: 'Laboratorio',
        dataIndex: 'lab',
        key: 'lab',
        sorter: (a, b) => a.lab - b.lab,
        sortOrder: sortedInfo.columnKey === 'lab' && sortedInfo.order,
      },
      {
        title: '%Laboratorio / Vendidos',
        dataIndex: 'porcenLab',
        key: 'porcenLab',
        sorter: (a, b) => a.porcenLab.split('%')[0] - b.porcenLab.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenLab' && sortedInfo.order,
      },
      {
        title: '%Procedimientos / Vendidos',
        dataIndex: 'porcenProcedimientos',
        key: 'porcenProcedimientos',
        sorter: (a, b) => a.porcenProcedimientos.split('%')[0] - b.porcenProcedimientos.split('%')[0],
        sortOrder: sortedInfo.columnKey === 'porcenProcedimientos' && sortedInfo.order,
      },
    ]
      .filter(columna => columnas.includes(columna.dataIndex))
      .sort((a, b) => columnas.indexOf(a.dataIndex) - columnas.indexOf(b.dataIndex));

    const headersCsv = headers
      .filter(columna => columnas.includes(columna.key))
      .sort((a, b) => columnas.indexOf(a.key) - columnas.indexOf(b.key));

    const dataCsv = data.map(row => {
      return {
        ...row,
        porcenBloqueados: row.porcenBloqueados.valor,
        porcenAsignacion: row.porcenAsignacion.valor,
        porcenAusentismo: row.porcenAusentismo.valor,
        porcenSobrecupo: row.porcenSobrecupo.valor
      }
    })
    return (
      <div className='marginTabla'>
        <Table
          pagination={{ pageSize: data.length, hideOnSinglePage: true }}
          scroll={{ x: columns.length * 90 }}
          columns={columns}
          dataSource={data}
          className='tabla'
          onChange={this.reordenar}
          loading={this.props.loading}
        >
        </Table>
        <CSVLink
          data={dataCsv}
          headers={headersCsv}
          style={downloadButton}
        >
          {'Descargar informe'.toUpperCase()}
        </CSVLink>
      </div >
    );
  }
}

export default Medico;