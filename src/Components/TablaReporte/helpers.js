export const headers = [
  {
    label: 'Nombre',
    key: 'Nombre'
  },
  {
    label: 'Programados',
    key: 'Programados'
  },
  {
    label: 'Bloqueados',
    key: 'Bloqueados'
  },
  {
    label: '%Bloqueos',
    key: 'porcenBloqueados'
  },
  {
    label: 'Disponibles',
    key: 'Disponibles'
  },
  {
    label: 'Asignados',
    key: 'Asignados'
  },
  {
    label: '%Asignación',
    key: 'porcenAsignacion'
  },
  {
    label: 'Ausentes',
    key: 'Ausentes'
  },
  {
    label: '%Ausentismo',
    key: 'porcenAusentismo'
  },
  {
    label: 'Sobrecupo',
    key: 'Sobrecupo'
  },
  {
    label: '%Sobrecupo',
    key: 'porcenSobrecupo'
  },
  {
    label: 'Recaudados',
    key: 'Recaudados'
  },
  {
    label: '%Vendido / Programado',
    key: 'porcenVendidoProgramado'
  },
  {
    label: '%Vendido / Disponible',
    key: 'porcenVendidoDisponible'
  },
  {
    label: 'T. Cirugias',
    key: 'cirugias',
  },
  {
    label: 'C. Programadas',
    key: 'programadas',
  },
  {
    label: 'C. Urgencias',
    key: 'urgencias',
  },
  {
    label: '%Cirugias / Vendidos',
    key: 'porcenCirugias',
  },
  {
    label: 'Procedimientos',
    key: 'procedimientos',
  },
  {
    label: 'Laboratorio',
    key: 'lab'
  }
];

export const downloadButton = {
  backgroundColor: '#009DB2',
  backgroundImage: 'linear-gradient(-236deg, #9cdae6 5%,#009db2 100%)',
  fontSize: 14,
  fontWeight: 500,
  width: '18%',
  padding: '0.5em',
  borderRadius: 5,
  color: '#fff',
  display: 'flex',
  justifyContent: 'center',
  boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.50)',
  marginTop: '10px',
  zIndex: 1000,
};