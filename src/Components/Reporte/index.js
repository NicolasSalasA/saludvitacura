import React, { Component } from 'react';
import { Radio, Button, Select, Spin } from 'antd';
import { connect } from 'react-redux';
import _ from 'lodash';
import axios from 'axios';
import TablaReporte from '../TablaReporte';
import { logOut } from '../../actions';
import { URL_API } from '../../config/constants';
import 'antd/dist/antd.css';
import './style.css';
import { ordenarAlfabeticamente } from '../../config/utils';

const RadioGroup = Radio.Group;
const Option = Select.Option;

class Reporte extends React.Component {
  constructor() {
    super()
    this.state = {
      especialidades: [],
      tipoReporte: 0,
      medicos: [],
      data: [],
      semanas: [],
      currentSemana: 0,
      fetched: false,
      loading: false,
      configReporte: 'consultas'
    }
  }

  componentDidMount() {
    this.getSemanas()
  }

  async getSemanas() {
    const { data: { semanas, columnas } } = await axios.get(`${URL_API}/getSemanas`);
    console.log(Object.keys(columnas))
    this.setState({
      semanas,
      columnas
    }, () => this.getDatosPorSemana(semanas[0].semanaAño))
  }

  async getDatosPorSemana(semana = "2018-23") {
    this.setState({ fetched: false, loading: true }, async () => {
      const {
        data: {
          r_esp_medico,
          r_especialidad,
          r_total
        }
      } = await axios.get(`${URL_API}/getResumen/${semana}`);
      const especialidades = _.keys(r_esp_medico).sort(ordenarAlfabeticamente);
      this.setState({
        especialidades,
        especialidad: especialidades[0],
        r_esp_medico,
        r_especialidad,
        r_total,
        fetched: true,
        loading: false,
        firstLoadCompleted: true
      }, this.actualizarData);
    })
  }

  actualizarData = () => {
    const {
      tipoReporte,
      r_esp_medico,
      r_especialidad,
      r_total,
      medicos,
    } = this.state;
    let data = [];
    switch (tipoReporte) {
      case 1: {
        const especialidad = r_especialidad[this.state.especialidad][0];
        const displayMedicos = medicos.map(index =>
          r_esp_medico[this.state.especialidad][index]);
        data = [
          ...displayMedicos,
          { ...especialidad, Nombre: especialidad.Especialidad },
          { ...r_total["Tabancura"][0], Nombre: "Red Salud Vitacura" }
        ]
        break;
      }
      case 2: {
        const especialidad = r_especialidad[this.state.especialidad][0];
        data = [
          ...r_esp_medico[this.state.especialidad],
          { ...especialidad, Nombre: especialidad.Especialidad },
          { ...r_total["Tabancura"][0], Nombre: "Red Salud Vitacura" }
        ]
        break;
      }
      case 3: {
        const especialidades = r_especialidad;
        data = [
          ...(_.values(especialidades).map(elem => ({ ...elem[0], Nombre: elem[0].Especialidad }))),
          { ...(r_total["Tabancura"][0]), Nombre: "Red Salud Vitacura" }
        ]
        break;
      }
      default: {
        break;
      }
    }
    this.setState({ data });
  }

  setTipoReporte = ({ target: { value: tipoReporte } }) => {
    this.setState({
      tipoReporte,
      medicos: []
    }, this.actualizarData);
  }

  setConfigReporte = (configReporte) => {
    this.setState({ configReporte });
  }

  setMedicos = (medicos) => {
    this.setState({
      medicos
    }, this.actualizarData);
  }

  setEspecialidad = (especialidad) => {
    this.setState({
      especialidad, medicos: []
    }, this.actualizarData)
  }

  setSemana = (currentSemana) => {
    this.setState({ currentSemana, medicos: [] }, () => this.getDatosPorSemana(this.state.semanas[currentSemana].semanaAño));
  }

  render() {
    const {
      tipoReporte,
      r_esp_medico,
      r_especialidad,
      r_total,
      medicos,
      especialidad,
      especialidades,
      data,
      semanas,
      currentSemana,
      fetched,
      loading,
      firstLoadCompleted,
      configReporte,
      columnas
    } = this.state;
    console.log(this.state);
    return (
      <div>
        <div>
          <header className='header'>
            <img src={require('../../assets/img//clinicatabancura.png')} />
            <div className=''>
              <Button
                className='cerrar'
                onClick={() => this.props.logOut()}
              >
                CERRAR SESIÓN
                            </Button>
            </div>
          </header>
        </div>
        {firstLoadCompleted ? <div className='margin'>
          <div className='first-input-group'>
            <div className='centro'>
              <label htmlFor="" className='campo'>Centro:</label>
              <label className=''>
                VITACURA
                            </label>
            </div>
            <div>
              <label className='campo'>
                Tipo Informe:
                            </label>
              <RadioGroup onChange={this.setTipoReporte} value={tipoReporte}>
                <Radio value={1}>Ficha Médico</Radio>
                <Radio value={2}>Medicos por Especialidad</Radio>
                <Radio value={3}>Total Especialidad</Radio>
              </RadioGroup>
            </div>
            <div className>
              <label className='campo'>
                Configurar Informe:
                            </label>
              <Select
                style={{ width: 300 }}
                placeholder="Configure informe"
                onChange={this.setConfigReporte}
                value={configReporte}
              >
                <Option
                  value={"todas"}
                  className='color'
                >
                  Todo
                                </Option>
                <Option
                  value={"consultas"}
                  className='color'
                >
                  Consultas
                                </Option>
                <Option
                  value={"cirugias"}
                  className='color'
                >
                  Cirugias
                                </Option>
                <Option
                  value={"examenes"}
                  className='color'
                >
                  Examenes
                                </Option>
                <Option
                  value={"procedimientos"}
                  className='color'
                >
                  Procedimientos
                                </Option>
                <Option
                  value={"laboratorio"}
                  className='color'
                >
                  Laboratorio
                                </Option>
                <Option
                  value={"ejecutivo"}
                  className='color'
                >
                  Resumen ejecutivo
                                </Option>
              </Select>
            </div>
          </div>
          <div className={tipoReporte != 2 ? 'select' : 'select-especialidad'}>
            {(tipoReporte !== 0) && <div className='center' >
              <label htmlFor="" className='campo'>Año-Semana</label>
              <div className=''>
                <Select
                  className=''
                  style={{ width: 220 }}
                  placeholder="Seleccione"
                  value={currentSemana}
                  onChange={this.setSemana}
                >
                  {semanas.map((semana, index) =>
                    <Option
                      className='color'
                      key={index}
                      value={index}
                    >
                      {semana.label}
                    </Option>
                  )}
                </Select>
              </div>
            </div>}
            {((tipoReporte === 2 || tipoReporte === 1) && fetched)
              && <div className=''>
                <label htmlFor="" className='campo'>Especialidad</label>
                <div className=''>
                  <Select style={{ width: 300 }}
                    placeholder="Seleccione Especialidad"
                    value={especialidad}
                    onChange={this.setEspecialidad}
                    className='color'
                    optionFilterProp="children"
                    showSearch
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {especialidades.map((especialidad, i) => <Option className='color' key={i} value={especialidad}>{especialidad}</Option>)}
                  </Select>
                </div>
              </div>}
            {(tipoReporte === 1 && fetched)
              && <div>
                <label htmlFor="" className='campo' >Medicos  </label>
                {especialidad && <div className=''>
                  <Select
                    mode="multiple"
                    allowClear
                    style={{ width: 500 }}
                    onChange={this.setMedicos}
                    value={medicos}
                    placeholder="Seleccione Medicos"
                    optionFilterProp="children"
                    showSearch
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {r_esp_medico[especialidad].map((medico, i) => <Option key={i}
                      value={i}>{medico.Nombre}</Option>)}
                  </Select>
                </div>}
              </div>}
          </div>
          <div>
            {data.length > 0 &&
              <TablaReporte
                data={data}
                especialidades={tipoReporte == 3 ? ["Red Salud Vitacura", ...especialidades] : []}
                columnas={columnas[configReporte]}
                loading={this.state.loading}
              />
            }
          </div>
        </div> : <Spin className="spinner" size="large" />}
      </div>

    );
  }
}

export default connect(null, { logOut })(Reporte);