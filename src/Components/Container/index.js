import React from 'react';
import { connect } from 'react-redux';
import { LocaleProvider } from 'antd';
import es_ES from 'antd/lib/locale-provider/es_ES';
import Login from '../Login';
import Reporte from '../Reporte';

const MainComponent = props => (
    <LocaleProvider locale={es_ES}>
        {!props.isLogged ? <Login /> : <Reporte />}
    </LocaleProvider>);

const mapStateToProps = ({ auth }) => {
    return {
        ...auth
    };
};

export default connect(mapStateToProps)(MainComponent);