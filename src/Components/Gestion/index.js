import React, { Component } from 'react';
import { Radio, Button, Select, Spin } from 'antd';
import { connect } from 'react-redux';
import Liquidacion from './Tablas/index';
import CobranzaIsapre from './Tablas/cobranzaIsapre';
import CobranzaPersonas from './Tablas/cobranzaPersonas';
import _ from 'lodash';
import axios from 'axios';
import { logOut } from '../../actions';
import { URL_API } from '../../config/constants';
import { Table } from 'antd';
import 'antd/dist/antd.css';
import './style.css';

const RadioGroup = Radio.Group;
const Option = Select.Option;

class Gestion extends React.Component {
    constructor() {
        super()
        this.state = {
            tipoInforme: 0,
            data: []
        }
    }

    componentDidMount() {

    }

    setInforme = (e) => {
        console.log('radio checked', e.target.value);
        this.setState({
            tipoInforme: e.target.value,
        });
    }
    render() {
        const {
            tipoInforme
        } = this.state;


        return (
            <div>
                <div>
                    <header className='header'>
                        <img src={require('../../assets/img//clinicatabancura.png')} />
                        <div className=''>
                            <Button
                                className='cerrar'
                                onClick={() => this.props.logOut()}
                            >
                                CERRAR SESIÓN
                            </Button>
                        </div>
                    </header>
                </div>
                <div className='margin'>
                    <label className='campo'>
                        Informe Finanzas:
                            </label>
                    <RadioGroup onChange={this.setInforme} value={this.state.tipoInforme}>
                        <Radio value={1}>Liquidacion</Radio>
                        <Radio value={2}>Cobranza Isapres</Radio>
                        <Radio value={3}>Cobranza Personas</Radio>
                    </RadioGroup>
                </div>
                <br />
                {(this.state.tipoInforme === 1) && <div >
                    <Liquidacion />
                </div>}
                {(this.state.tipoInforme === 2) && <div>
                    <CobranzaIsapre />
                </div>}
                {(this.state.tipoInforme === 3) && <div>
                    <CobranzaPersonas />
                </div>}

            </div>

        );
    }
}

export default connect(null, { logOut })(Gestion);