import React, { Component } from 'react';
import { Radio, Button, Select, Spin } from 'antd';
import { connect } from 'react-redux';
import { headers, ordenarAlfebeticamente, downloadButton } from '../helpers';
import _ from 'lodash';
import axios from 'axios';
import { logOut } from '../../../actions';
import { URL_API } from '../../../config/constants';
import { Table } from 'antd';
import 'antd/dist/antd.css';

const RadioGroup = Radio.Group;
const Option = Select.Option;

class cobranzaIsapre extends React.Component {
    constructor() {
        super()
    }

    componentDidMount() {

    }

    render() {

        const stockCuentas = [{
            title: 'Stock de cuentas en Isapre',
            dataIndex: 'stock',
            key: 'stock'
        }, {
            title: 'Hoy[$]',
            dataIndex: 'hoy',
            key: 'hoy',
        }, {
            title: 'Ayer[$]',
            dataIndex: 'ayer',
            key: 'ayer'
        }, {
            title: 'Hoy[N°]',
            dataIndex: 'hoynumero',
            key: 'hoynumero'

        }, , {
            title: 'Ayer[N°]',
            dataIndex: 'ayernumero',
            key: 'ayernumero'

        }

        ]
        const columns = [{
            title: 'Stock de cuentas en liquidacion',
            dataIndex: 'stock',
            key: 'stock'
        }, {
            title: '[0-29 Días]',
            dataIndex: 'cero29dias',
            key: 'cero29dias',
        }, {
            title: '[30-59 Días]',
            dataIndex: 'treinta59dias',
            key: 'treinta59dias'
        }, {
            title: '[60-89 Días]',
            dataIndex: 'sesenta89dias',
            key: 'sesenta89dias'

        }, {
            title: '[90-179 Días]',
            dataIndex: 'noventa179dias',
            key: 'noventa179dias'

        }, {
            title: '[<180 dias]',
            dataIndex: 'mayorDias',
            key: 'mayorDias'

        }, {
            title: 'Total',
            dataIndex: 'total',
            key: 'total'

        }];



        const dataAntiguedadCuenta = [{
            key: '1',
            stock: 'Cuenta derivada a Isapre',
            cero29dias: '$ 4.686.399.218',
            treinta59dias: '$ 1.943.934.390',
            sesenta89dias: '  $ 537.545.111',
            noventa179dias: '	$ 760.309.133',
            mayorDias: '$ 619.739.700',
            total: '$ 8.547.927.552',
        }, {
            key: '2',
            stock: 'Cuentas derivadas a Onco.',
            cero29dias: '$ 2.344.207.627',
            treinta59dias: '$ 0',
            sesenta89dias: '$ 0',
            noventa179dias: '$ 364.336',
            mayorDias: '$ 6.414.179	',
            total: '$ 2.350.986.142',
        }, {
            key: '3',
            stock: 'Total CxC Isapre',
            cero29dias: '$ 7.030.606.845',
            treinta59dias: '$ 1.943.934.390',
            sesenta89dias: '$ 537.545.111',
            noventa179dias: '$ 760.673.469',
            mayorDias: '$ 626.153.879',
            total: '$ 10.898.913.694',
        }]
        const cuentasIsapre = [{
            key: '1',
            stock: 'ISAPRE CRUZ BLANCA',
            cero29dias: '$971.992.620',
            treinta59dias: '$977.991.345',
            sesenta89dias: '$977.991.345',
            noventa179dias: '$977.991.345',
            mayorDias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            total: '$977.991.345',
        }, {
            key: '2',
            stock: 'ISAPRE BANMEDICA S.A',
            cero29dias: '$971.992.620',
            treinta59dias: '$977.991.345',
            sesenta89dias: '$977.991.345',
            noventa179dias: '$977.991.345',
            mayorDias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            total: '$977.991.345',
        },
        {
            key: '3',
            stock: 'ISAPRE CONSALUD',
            cero29dias: '$971.992.620',
            treinta59dias: '$977.991.345',
            sesenta89dias: '$977.991.345',
            noventa179dias: '$977.991.345',
            mayorDias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            total: '$977.991.345',
        }]
        const dataStockCuentas = [
            {
                key: '1',
                stock: 'Cuenta derivada a Isapre',
                hoy: '	$ 8.547.927.552',
                ayer: '$ 8.554.979.042',
                hoynumero: '2.151',
                ayernumero: '2.137',
            }, {
                key: '2',
                stock: ' Cuentas derivadas a Onco.',
                hoy: '$ 2.350.986.142',
                ayer: '	$ 2.379.120.507',
                hoynumero: '1.199',
                ayernumero: '1.214',
            }, {
                key: '3',
                stock: 'Total CxC Isapre',
                hoy: '$ 10.898.913.694',
                ayer: '$ 10.934.099.549',
                hoynumero: '3.350',
                ayernumero: '3.351',
            }]
        const stockInicioColumns = [{
            title: 'Stock de Cuentas en Isapre',
            dataIndex: 'stock',
        }, {
            title: 'Hoy',
            dataIndex: 'hoy',
        }, {
            title: 'Ayer',
            dataIndex: 'ayer',
        }];
        const dataStockInicio = [{
            key: '1',
            stock: 'Cuenta derivada a Isapre',
            hoy: '$8.547.927.552',
            ayer: '$8.554.979.042',
        }, {
            key: '2',
            stock: 'Cuentas derivadas a Onco.',
            hoy: '$2.350.986.142	',
            ayer: '$2.379.120.507',
        }, {
            key: '3',
            stock: 'Tramite en Isapre',
            hoy: ' $10.898.913.694',
            ayer: '$10.934.099.549',
        }];
        return (
            <div className='margin'>
                <div>
                    <h4><u><strong>3.0 Cobranza Temprana Isapres(Pre - Bonificación)</strong></u></h4>
                    <Table columns={stockInicioColumns} dataSource={dataStockInicio} size="middle" />
                </div>
                <div>
                    <h4><u><strong>3.1 Stock de cuentas en isapre</strong></u></h4>
                    <Table columns={stockCuentas} dataSource={dataStockCuentas} size="middle" />
                </div>
                <div>
                    <h4><u><strong>3.2 Antiguedad de las cuentas</strong></u></h4>
                    <Table columns={columns} dataSource={dataAntiguedadCuenta} size="middle" />
                </div>
                <div>
                    <h4><u><strong>3.3 Cuentas por Isapre</strong></u></h4>
                    <Table columns={columns} dataSource={cuentasIsapre} size="middle" />
                </div>
            </div>

        );
    }
}

export default connect(null, { logOut })(cobranzaIsapre);


