import React, { Component } from 'react';
import { Radio, Button, Select, Spin } from 'antd';
import { connect } from 'react-redux';
import { headers, ordenarAlfebeticamente, downloadButton } from '../helpers';
import _ from 'lodash';
import axios from 'axios';
import { logOut } from '../../../actions';
import { URL_API } from '../../../config/constants';
import { Table } from 'antd';
import 'antd/dist/antd.css';

const RadioGroup = Radio.Group;
const Option = Select.Option;

class cobranzaPersonas extends React.Component {
    constructor() {
        super()
    }

    componentDidMount() {

    }

    render() {

        const stockCuentas = [{
            title: 'Stock de cuentas en Cobranza temprana',
            dataIndex: 'stock',
            key: 'stock'
        }, {
            title: 'Hoy[$]',
            dataIndex: 'hoy',
            key: 'hoy',
        }, {
            title: 'Ayer[$]',
            dataIndex: 'ayer',
            key: 'ayer'
        }, {
            title: 'Hoy[N°]',
            dataIndex: 'hoynumero',
            key: 'hoynumero'

        }, , {
            title: 'Ayer[N°]',
            dataIndex: 'ayernumero',
            key: 'ayernumero'

        }

        ]
        const columns = [{
            title: 'Stock de Cuentas en Cobranza temprana',
            dataIndex: 'stock',
            key: 'stock'
        }, {
            title: '[0-29 Días]',
            dataIndex: 'cero29dias',
            key: 'cero29dias',
        }, {
            title: '[30-59 Días]',
            dataIndex: 'treinta59dias',
            key: 'treinta59dias'
        }, {
            title: '[60-89 Días]',
            dataIndex: 'sesenta89dias',
            key: 'sesenta89dias'

        }, {
            title: '[90-179 Días]',
            dataIndex: 'noventa179dias',
            key: 'noventa179dias'

        }, {
            title: '[<180 dias]',
            dataIndex: 'mayorDias',
            key: 'mayorDias'

        }, {
            title: 'Total',
            dataIndex: 'total',
            key: 'total'

        }];
        const columnsGrandesDeudores = [
            {
                title: 'Nombre Paciente',
                dataIndex: 'nombre',
                key: 'nombre'
            }, {
                title: 'Monto de la Cuenta [$]',
                dataIndex: 'monto',
                key: 'monto'
            }, {
                title: 'Asegurador',
                dataIndex: 'asegurador',
                key: 'asegurador'
            }, {
                title: 'Días desde Egreso',
                dataIndex: 'diasEgreso',
                key: 'diasEgreso'
            }, {
                title: 'Estado',
                dataIndex: 'estado',
                key: 'estado'
            }]
        const dataGrandesDeudores = [{
            key: '1',
            nombre: 'MARISOL MARGARITA PONCE BENITEZ',
            monto: '$ 138.551.349',
            asegurador: 'FONASA',
            diasEgreso: '686',
            estado: 'COPAGO'
        }, {
            key: '2',
            nombre: 'HERNAN ALBERTO ESCARATE ESCARATE',
            monto: '$ 54.777.763',
            asegurador: 'ISAPRE CRUZ BLANCA',
            diasEgreso: '2',
            estado: 'PACIENTE'
        }, {
            key: '3',
            nombre: 'TOMAS CARLOS SALINAS SANCHEZ',
            monto: '$ 42.341.036',
            asegurador: 'FONASA',
            diasEgreso: '203',
            estado: 'COPAGO'
        }, {
            key: '3',
            nombre: 'HARRISON THOMAS CLARKE WINTER',
            monto: '$ 36.900.011',
            asegurador: 'ISAPRE CONSALUD	',
            diasEgreso: '196',
            estado: 'PAGADA'
        }, {
            key: '4',
            nombre: 'Total CxC Isapre',
            monto: '$ 485.427.480'
        }]


        const columnsIsapre = [{
            title: 'Stock de Cuentas en Isapre',
            dataIndex: 'stock',
            key: 'stock'
        }, {
            title: '[0-29 Días]',
            dataIndex: 'cero29dias',
            key: 'cero29dias',
        }, {
            title: '[30-59 Días]',
            dataIndex: 'treinta59dias',
            key: 'treinta59dias'
        }, {
            title: '[60-89 Días]',
            dataIndex: 'sesenta89dias',
            key: 'sesenta89dias'

        }, {
            title: '[90-179 Días]',
            dataIndex: 'noventa179dias',
            key: 'noventa179dias'

        }, {
            title: '[<180 dias]',
            dataIndex: 'mayorDias',
            key: 'mayorDias'

        }, {
            title: 'Total',
            dataIndex: 'total',
            key: 'total'

        }];



        const dataAntiguedadCuenta = [{
            key: '1',
            stock: 'Cuenta derivada a Isapre',
            cero29dias: '$ 4.686.399.218',
            treinta59dias: '$ 1.943.934.390',
            sesenta89dias: '  $ 537.545.111',
            noventa179dias: '	$ 760.309.133',
            mayorDias: '$ 619.739.700',
            total: '$ 8.547.927.552',
        }, {
            key: '2',
            stock: 'Cuentas derivadas a Onco.',
            cero29dias: '$ 2.344.207.627',
            treinta59dias: '$ 0',
            sesenta89dias: '$ 0',
            noventa179dias: '$ 364.336',
            mayorDias: '$ 6.414.179	',
            total: '$ 2.350.986.142',
        }, {
            key: '3',
            stock: 'Total CxC Isapre',
            cero29dias: '$ 7.030.606.845',
            treinta59dias: '$ 1.943.934.390',
            sesenta89dias: '$ 537.545.111',
            noventa179dias: '$ 760.673.469',
            mayorDias: '$ 626.153.879',
            total: '$ 10.898.913.694',
        }]
        const cuentasIsapre = [{
            key: '1',
            stock: 'ISAPRE CRUZ BLANCA',
            cero29dias: '$971.992.620',
            treinta59dias: '$977.991.345',
            sesenta89dias: '$977.991.345',
            noventa179dias: '$977.991.345',
            mayorDias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            total: '$977.991.345',
        }, {
            key: '2',
            stock: 'ISAPRE BANMEDICA S.A',
            cero29dias: '$971.992.620',
            treinta59dias: '$977.991.345',
            sesenta89dias: '$977.991.345',
            noventa179dias: '$977.991.345',
            mayorDias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            total: '$977.991.345',
        },
        {
            key: '3',
            stock: 'ISAPRE CONSALUD',
            cero29dias: '$971.992.620',
            treinta59dias: '$977.991.345',
            sesenta89dias: '$977.991.345',
            noventa179dias: '$977.991.345',
            mayorDias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            total: '$977.991.345',
        }]
        const dataStockCuentas = [
            {
                key: '1',
                stock: 'Cuenta derivada a Isapre',
                hoy: '	$ 8.547.927.552',
                ayer: '$ 8.554.979.042',
                hoynumero: '2.151',
                ayernumero: '2.137',
            }, {
                key: '2',
                stock: ' Cuentas derivadas a Onco.',
                hoy: '$ 2.350.986.142',
                ayer: '	$ 2.379.120.507',
                hoynumero: '1.199',
                ayernumero: '1.214',
            }, {
                key: '3',
                stock: 'Total CxC Isapre',
                hoy: '$ 10.898.913.694',
                ayer: '$ 10.934.099.549',
                hoynumero: '3.350',
                ayernumero: '3.351',
            }]
        const stockInicioColumns = [{
            title: 'Stock de cuentas en cobranza temprana',
            dataIndex: 'stock',
        }, {
            title: 'Hoy',
            dataIndex: 'hoy',
        }, {
            title: 'Ayer',
            dataIndex: 'ayer',
        }];






        const dataStockInicio = [{
            key: '1',
            stock: 'Copago Paciente',
            hoy: '$2.919.113.914',
            ayer: '$2.897.730.507',
        }, {
            key: '2',
            stock: 'Cuenta Pagada',
            hoy: '$5.273.145.589',
            ayer: '$5.271.328.148',
        }, {
            key: '3',
            stock: 'Paciente',
            hoy: '$1.292.520.302',
            ayer: '$1.369.526.946',
        }, {
            key: '4',
            stock: 'Recaudación Copago',
            hoy: '$9.484.779.805',
            ayer: '$9.538.585.601',
        }];
        return (
            <div className='margin'>
                <div>
                    <h4><u><strong>4.0 Cobranza Temprana Personas (Post-Bonificación)</strong></u></h4>
                    <Table columns={stockInicioColumns} dataSource={dataStockInicio} size="middle" />
                </div>
                <div>
                    <h4><u><strong>4.1 Stock de cuentas en isapre</strong></u></h4>
                    <Table columns={stockCuentas} dataSource={dataStockCuentas} size="middle" />
                </div>
                <div>
                    <h4><u><strong>4.2 Antiguedad de las cuentas</strong></u></h4>
                    <Table columns={columns} dataSource={dataAntiguedadCuenta} size="middle" />
                </div>
                <div>
                    <h4><u><strong>4.3 Cuentas por Isapre</strong></u></h4>
                    <Table columns={columnsIsapre} dataSource={cuentasIsapre} size="middle" />
                </div>
                <div>
                    <h4><u><strong>4.4 Grandes Cuentas</strong></u></h4>
                    <Table columns={columnsGrandesDeudores} dataSource={dataGrandesDeudores} size="middle" />
                </div>
            </div>

        );
    }
}

export default connect(null, { logOut })(cobranzaPersonas);


