import React, { Component } from 'react';
import { Radio, Button, Select, Spin } from 'antd';
import { connect } from 'react-redux';
import { headers, ordenarAlfebeticamente, downloadButton } from '../helpers';
import _ from 'lodash';
import axios from 'axios';
import { logOut } from '../../../actions';
import { URL_API } from '../../../config/constants';
import { Table } from 'antd';
import 'antd/dist/antd.css';
import './style.css'

const RadioGroup = Radio.Group;
const Option = Select.Option;

class Liquidacion extends React.Component {
    constructor() {
        super()
    }

    componentDidMount() {

    }

    render() {

        const stockCuentas = [{
            title: 'Stock de cuentas en liquidacion',
            dataIndex: 'stock',
            key: 'stock'
        }, {
            title: 'Hoy[$]',
            dataIndex: 'hoy',
            key: 'hoy',
        }, {
            title: 'Ayer[$]',
            dataIndex: 'ayer',
            key: 'ayer'
        }, {
            title: 'Hoy[N°]',
            dataIndex: 'hoynumero',
            key: 'hoynumero'

        }, , {
            title: 'Ayer[N°]',
            dataIndex: 'ayernumero',
            key: 'ayernumero'

        }

        ]
        const columns = [{
            title: 'Stock de cuentas en liquidacion',
            dataIndex: 'stock',
            key: 'stock'
        }, {
            title: '[0-4 Días]',
            dataIndex: 'cero4dias',
            key: 'cero4dias',
        }, {
            title: '[5-9 Días]',
            dataIndex: 'cinco9dias',
            key: 'cinco9dias'
        }, {
            title: '[10-14 Días]',
            dataIndex: 'diez14dias',
            key: 'diez14dias'

        }, {
            title: '[15-19 Días]',
            dataIndex: 'quince19dias',
            key: 'quince19dias'

        }, {
            title: '[20-24 Días]',
            dataIndex: 'veinte24dias',
            key: 'veinte24dias'

        }, {
            title: '[25-30 Días]',
            dataIndex: 'veinticinco30dias',
            key: 'veinticinco30dias'

        }, {
            title: '[>30 Días]',
            dataIndex: 'dias',
            key: 'dias'

        }, {
            title: 'Total',
            dataIndex: 'total',
            key: 'total'

        },];



        const dataAntiguedadCuenta = [{
            key: '1',
            stock: 'Cuenta liquida',
            cero4dias: '$971.992.620',
            cinco9dias: '$977.991.345',
            diez14dias: '$977.991.345',
            quince19dias: '$977.991.345',
            veinte24dias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            dias: '$977.991.345',
            total: '$977.991.345',
        }, {
            key: '2',
            stock: 'Paciente con alta, sin tener la cuenta cerrada',
            cero4dias: '$971.992.620',
            cinco9dias: '$977.991.345',
            diez14dias: '$977.991.345',
            quince19dias: '$977.991.345',
            veinte24dias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            dias: '$977.991.345',
            total: '$977.991.345',
        },
        {
            key: '3',
            stock: 'Cuentas devueltas',
            cero4dias: '$971.992.620',
            cinco9dias: '$977.991.345',
            diez14dias: '$977.991.345',
            quince19dias: '$977.991.345',
            veinte24dias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            dias: '$977.991.345',
            total: '$977.991.345',
        }]
        const data2 = [{
            key: '1',
            stock: 'ISAPRE CRUZ BLANCA',
            cero4dias: '$971.992.620',
            cinco9dias: '$977.991.345',
            diez14dias: '$977.991.345',
            quince19dias: '$977.991.345',
            veinte24dias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            dias: '$977.991.345',
            total: '$977.991.345',
        }, {
            key: '2',
            stock: 'ISAPRE BANMEDICA S.A',
            cero4dias: '$971.992.620',
            cinco9dias: '$977.991.345',
            diez14dias: '$977.991.345',
            quince19dias: '$977.991.345',
            veinte24dias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            dias: '$977.991.345',
            total: '$977.991.345',
        },
        {
            key: '3',
            stock: 'ISAPRE CONSALUD',
            cero4dias: '$971.992.620',
            cinco9dias: '$977.991.345',
            diez14dias: '$977.991.345',
            quince19dias: '$977.991.345',
            veinte24dias: '$977.991.345',
            veinticinco30dias: '$977.991.345',
            dias: '$977.991.345',
            total: '$977.991.345',
        }]
        const dataStockCuentas = [
            {
                key: '1',
                stock: 'Paciente con alta, sin tener la cuenta cerrada',
                hoy: '$ 888.296.809',
                ayer: '$ 971.992.620',
                hoynumero: '1.088',
                ayernumero: '1.114',
            }, {
                key: '2',
                stock: 'Cuenta liquidada',
                hoy: '$ 2.000.319.577',
                ayer: '$ 1.868.213.456',
                hoynumero: '648',
                ayernumero: '614',
            }, {
                key: '3',
                stock: 'Paciente actualmente hospitalizado',
                hoy: '$ 777.214.598	',
                ayer: '$ 708.865.289',
                hoynumero: '170	',
                ayernumero: '157',
            }, {
                key: '4',
                stock: 'Cuentas devueltas',
                hoy: '$ 245.563.224	',
                ayer: '$ 256.342.670',
                hoynumero: '63',
                ayernumero: '67',
            }, {
                key: '5',
                stock: 'Total CxC Liquidación',
                hoy: '$ 3.911.394.208',
                ayer: '$ 3.805.414.035',
                hoynumero: '1.969',
                ayernumero: '1.952',
            }]
        const stockInicioColumns = [{
            title: 'Stock de cuentas en liquidacion',
            dataIndex: 'stock',
        }, {
            title: 'Hoy',
            dataIndex: 'today',
        }, {
            title: 'Ayer',
            dataIndex: 'yesterday',
        }];
        const dataStockInicio = [{
            key: '1',
            stock: 'Cuenta liquidada',
            today: '$2.000.319.577',
            yesterday: '$1.868.213.456',

        }, {
            key: '2',
            stock: 'Paciente con alta, sin tener la cuenta cerrada',
            today: '$971.992.620',
            yesterday: '$977.991.345',
        }, {
            key: '3',
            stock: 'Paciente actualmente hospitalizado	',
            today: '$708.865.289',
            yesterday: '$671.584.939',
        }, {
            key: '4',
            stock: 'Cuentas devueltas ',
            today: '$256.342.670',
            yesterday: '$240.877.667',
        }, {
            key: '5',
            stock: 'Cuentas devueltas ',
            today: '$1.868.213.456	',
            yesterday: '$1.973.330.859',
        }, {
            key: '6',
            stock: 'Total CxC Liquidación ',
            today: '$3.805.414.035',
            yesterday: '$3.863.784.810'
        }];
        return (
            <div className='margin'>
                <div>
                    <h4><u><strong>2.0 Liquidaciónes</strong></u></h4>
                    <Table className='' columns={stockInicioColumns} dataSource={dataStockInicio} size="middle" />
                </div>
                <div>
                    <h4><u><strong>2.1 Stock de cuentas</strong></u></h4>
                    <Table columns={stockCuentas} dataSource={dataStockCuentas} size="middle" />
                </div>
                <div>
                    <h4><u><strong>2.2 Antiguedad de las cuentas</strong></u></h4>
                    <Table columns={columns} dataSource={dataAntiguedadCuenta} size="middle" />
                </div>
                <div>
                    <h4><u><strong>2.3Cuentas por Isapre</strong></u></h4>
                    <Table columns={columns} dataSource={data2} size="middle" />
                </div>
            </div>

        );
    }
}

export default connect(null, { logOut })(Liquidacion);
