export const headers = [
    {
        //PacAlta = Paciente alta
        label: 'Stock de cuentas en liquidacion	',
        key: 'stock'
    },
    {
        label: 'Hoy',
        key: 'hoy'
    },
    {
        label: 'Ayer',
        key: 'ayer'
    },
    {
        label: 'Stock de Cuentas en Isapre',
        key: 'stockIsapre'
    },
    {
        label: 'Stock de cuentas en cobranza tempranaxd',
        key: 'stockCobrnza'
    },
    {
        label: 'Hoy[$]',
        key: 'hoyPeso'
    },
    {
        label: 'Ayer[$]',
        key: 'ayerPeso'
    },
    {

        label: 'Hoy[N°]',
        key: 'hoyNumero'
    },
    {
        label: 'Ayer[N°]',
        key: 'ayerNumero'
    },
    {

        label: '[0-4 Días]',
        key: 'cero4dias'
    },
    {
        label: '[5-9 Días]',
        key: 'cinco9dias'
    },
    {
        label: '[10-14 Días]',
        key: 'diez14dias'
    },
    {
        label: '[15-19 Días]',
        key: 'quince19dias'
    },
    {
        label: '[20-24 Días]',
        key: 'veinte24dias'
    },
    {
        label: '[25-30 Días]',
        key: 'veinticinco30dias'
    },
    {
        label: '[>30 Días]',
        key: '30dias'
    },
    {
        label: 'Total',
        key: 'total'
    },
    {

        label: 'Nombre Paciente',
        key: 'nombrePaciente'
    },
    {

        label: 'Monto de la Cuenta [$]',
        key: 'montoCuenta'
    },
    {
        label: 'Días desde Egreso',
        key: 'diasEgreso'
    },
    {
        label: 'Estado',
        key: 'estado'
    },
    {

        label: 'Días desde Egreso',
        key: 'diasEgreso'
    }
];

export const downloadButton = {
    backgroundColor: '#009DB2',
    backgroundImage: 'linear-gradient(-236deg, #9cdae6 5%,#009db2 100%)',
    fontSize: 14,
    fontWeight: 500,
    width: '18%',
    padding: '0.5em',
    borderRadius: 5,
    color: '#fff',
    display: 'flex',
    justifyContent: 'center',
    boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.50)',
    marginTop: '-50px',
    zIndex: 1000,
};