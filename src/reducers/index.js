import { combineReducers } from 'redux';
import { LOGIN_USER, LOGOUT } from '../config/constants';

const auth = (state = { loading: true, isLogged: false }, action) => {
    switch (action.type) {
        case LOGIN_USER: {
            return { ...state, isLogged: true };
        }
        case LOGOUT: {
            return { ...state, isLogged: false };
        }
        default:
            return state;
    }
};

export default combineReducers({
    auth
});
